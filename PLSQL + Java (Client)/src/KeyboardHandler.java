import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class KeyboardHandler {

    private Client client;

    KeyboardHandler(GridPane grid, Client client)
    {
        this.client = client;
        grid.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                handleEvent(event);
            }
        });
    }

    private void handleEvent(KeyEvent e)
    {
        KeyCode code = e.getCode();
        String key = "";

        if (e.getEventType() == KeyEvent.KEY_PRESSED && code == KeyCode.W)
        {
            key = "Up";
        }
        if (e.getEventType() == KeyEvent.KEY_PRESSED && code == KeyCode.A)
        {
            key = "Left";
        }
        if (e.getEventType() == KeyEvent.KEY_PRESSED && code == KeyCode.S)
        {
            key = "Down";
        }
        if (e.getEventType() == KeyEvent.KEY_PRESSED && code == KeyCode.D)
        {
            key = "Right";
        }

        try
        {
            Thread.sleep(300);
        }
        catch (InterruptedException e1)
        {
            e1.printStackTrace();
        }
        client.keyPressed(key);
    }
}
