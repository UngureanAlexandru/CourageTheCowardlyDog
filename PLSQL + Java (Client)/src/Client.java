import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.event.EventHandler;
import javafx.util.Duration;
import sun.rmi.runtime.Log;

import java.io.*;
import java.net.Socket;

public class Client extends Application{

    public static int x;
    public static int y;

    public static int targetX;
    public static int targetY;
    public static boolean readyToUpdate = false;

    private final int mapWidth = 25;
    private final int mapHeight = 10;

    private Socket clientSocket;
    private BufferedReader reader;
    private BufferedWriter writer;

    private Map map;
    private KeyboardHandler keyboardHandler;

    private GridPane gamePane;
    private GridPane loginPane;
    private GridPane registerPane;

    private Scene gameScene;
    private Scene loginScene;
    private Scene registerScene;

    private int score;
    private int gamesPlayed;

    private Label scoreLabel;
    public static boolean targetExists = false;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try
        {
            x = 10;
            y = 5;

            targetX = -1;
            targetY = -1;

            gamePane = new GridPane();
            gamePane.setPrefSize(1300,600);

            loginPane = new GridPane();
            loginPane.setPrefSize(1300,600);

            registerPane = new GridPane();
            registerPane.setPrefSize(1300,600);

            loginScene =  new Scene(loginPane, 1300, 600);
            gameScene = new Scene(gamePane, 1300, 600);
            registerScene = new Scene(registerPane, 1300, 600);

            clientSocket = new Socket("127.0.0.1", 3000);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            //////////////////////// Game Canvas ///////////////////////

            map = new Map(gamePane, mapWidth, mapHeight);

            primaryStage.setTitle("Courage, the game");
            Button button = new Button("Save and exit");
            scoreLabel = new Label("Score : 0");
            button.setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    exit();
                }
            });

            gamePane.add(button, 0 ,0);
            gamePane.add(scoreLabel, 5 ,0);

            //////////////////////////////// Login Canvas //////////////////////////

            loginPane.getColumnConstraints().add(new ColumnConstraints(100));
            loginPane.getColumnConstraints().add(new ColumnConstraints(100));
            loginPane.getColumnConstraints().add(new ColumnConstraints(200));


            Label loginTitle = new Label("Login");
            Label usernameLabel = new Label("Username:");
            TextField usernameText = new TextField();
            Label passwordLabel = new Label("Password:");
            PasswordField passwordText = new PasswordField();
            Button loginButton = new Button("Login");
            Button goToRegisterButton = new Button("Register");

            loginPane.add(loginTitle, 2, 0);
            loginPane.add(usernameLabel, 1, 1);
            loginPane.add(usernameText, 2, 1);
            loginPane.add(passwordLabel, 1, 2);
            loginPane.add(passwordText, 2, 2);
            loginPane.add(loginButton, 1, 3);
            loginPane.add(goToRegisterButton, 2, 3);

            loginButton.setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    if(login(usernameText.getText(), passwordText.getText()))
                    {
                        startGame(primaryStage);
                    }
                }
            });

            goToRegisterButton.setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    primaryStage.setScene(registerScene);
                    primaryStage.show();
                }
            });

            //////////////////////////////// Register Canvas //////////////////////////

            registerPane.getColumnConstraints().add(new ColumnConstraints(100));
            registerPane.getColumnConstraints().add(new ColumnConstraints(100));
            registerPane.getColumnConstraints().add(new ColumnConstraints(200));


            Label registerTitle = new Label("Register");
            Label registerUsernameLabel = new Label("Username:");
            TextField registerUsernameText = new TextField();
            Label registerPasswordLabel = new Label("Password:");
            PasswordField registerPasswordText = new PasswordField();
            Button goToLoginButton = new Button("Back");
            Button registerButton = new Button("Register");

            goToLoginButton.setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    primaryStage.setScene(loginScene);
                    primaryStage.show();
                }
            });

            registerButton.setOnAction(new EventHandler<ActionEvent>()
            {
                @Override
                public void handle(ActionEvent event)
                {
                    register(registerUsernameText.getText(), registerPasswordText.getText());
                }
            });

            registerPane.add(registerTitle, 2, 0);
            registerPane.add(registerUsernameLabel, 1, 1);
            registerPane.add(registerUsernameText, 2, 1);
            registerPane.add(registerPasswordLabel, 1, 2);
            registerPane.add(registerPasswordText, 2, 2);
            registerPane.add(goToLoginButton, 2, 3);
            registerPane.add(registerButton, 1, 3);

            //////////////////////////////////////////////////////////
            primaryStage.setScene(loginScene);
            primaryStage.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean login(String username, String password)
    {
        try
        {
            writer.write("login\n");
            writer.flush();

            String data = username + "," + password + "\n";
            writer.write(data);
            writer.flush();

            String result = reader.readLine();

            System.out.println( "Res "+ result);

            if (!result.equals("noData"))
            {
                String[] fromServer = result.split(",");
                score = Integer.parseInt(fromServer[3]);
                LogicThread.id = Integer.parseInt(fromServer[0]);
                return  true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    private boolean register(String username, String password)
    {
        try
        {
            writer.write("register\n");
            writer.flush();

            String data = username + "," + password + "\n";
            writer.write(data);
            writer.flush();

            String result = reader.readLine();
            System.out.println( "Res "+ result);

            if (!result.equals("Error!"))
            {
                return  true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    private void startGame(Stage primaryStage)
    {
        scoreLabel.setText("Score: " + score);

        LogicThread.active = true;

        LogicThread logicThread = new LogicThread(this, clientSocket, map);
        new Thread(logicThread).start();

        keyboardHandler = new KeyboardHandler(gamePane, this);

        primaryStage.setScene(gameScene);
        primaryStage.show();

        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(500),
                ae -> parsePositions()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void exit()
    {
        LogicThread.active = false;
        gamesPlayed++;
        String data = score + "," +  gamesPlayed + "\n";

        try
        {
            writer.write(data);
            writer.flush();

            clientSocket.close();
            reader.close();
            writer.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        System.exit(0);
    }

    public void keyPressed(String key)
    {
        if (key.equals("Up"))
        {
            if(y > 1)
            {
                y--;
            }
        }
        else  if (key.equals("Left"))
        {
            if (x > 0)
            {
                x--;
            }
        }
        else  if (key.equals("Down"))
        {
            if (y < map.getHeight())
            {
                y++;
            }
        }
        else  if (key.equals("Right"))
        {
            if (x < map.getWidth() - 1)
            {
                x++;
            }
        }
        parsePositions();

        map.move(gamePane, x, y, key);
        Client.readyToUpdate = true;
    }

    private void parsePositions()
    {
        if (targetX > 0 && targetY > 0)
        {
            targetExists = true;
            map.drawTarget(targetX, targetY);

            if (
                    x + 1 == targetX && y == targetY ||
                    x - 1 == targetX && y == targetY ||
                    x == targetX && y + 1 == targetY ||
                    x == targetX && y - 1 == targetY ||
                    x == targetX && y == targetY
                )
            {
                score++;
                targetExists = false;
                scoreLabel.setText("Score: " + score);
                targetX = -1;
                targetY = -1;
            }
        }

        if (LogicThread.lastPositions != null)
        {
            for (int i = 0; i < LogicThread.lastPositions.length; i++)
            {
                String[] lastPosition = LogicThread.lastPositions[i].split(",");

                int x = Integer.parseInt(lastPosition[1]);
                int y = Integer.parseInt(lastPosition[2]);

                map.erase(x, y);

                if (x < mapWidth - 1)
                {
                    map.erase(x + 1, y);
                }

                if (x > 0)
                {
                    map.erase(x - 1, y);
                }

                if (y < mapHeight - 1)
                {
                    map.erase(x, y + 1);
                }

                if (y > 1)
                {
                    map.erase(x, y - 1);
                }
            }

        }

        for (int i = 0; i < LogicThread.playersPositions.length; i++)
        {
            String[] position = LogicThread.playersPositions[i].split(",");
            int id = Integer.parseInt(position[0]);
            int x = Integer.parseInt(position[1]);
            int y = Integer.parseInt(position[2]);

            if (LogicThread.id != id)
            {
                drawElement(0, x, y);
            }
            else if (LogicThread.id == id)
            {
                drawElement(1, x, y);
            }
        }
    }

    private void drawElement(int type, int x, int y)
    {
        if (type == 0)
        {
            map.drawOtherPLayer(x, y);
        }
        else if (type == 1)
        {
            map.drawClient(x, y);
        }
    }
}
