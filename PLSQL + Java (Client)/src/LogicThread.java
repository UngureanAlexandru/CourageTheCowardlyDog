
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class LogicThread implements Runnable {

    public static boolean active = true;
    private Socket clientSocket;
    public static int id;
    private Map map;
    public static String[] playersPositions;
    public static String[] lastPositions;

    private Client client;

    private BufferedReader reader;
    private BufferedWriter writer;

    LogicThread(Client client, Socket clientSocket, Map map)
    {
        this.map = map;
        this.client = client;
        lastPositions = null;

        this.clientSocket = clientSocket;
        try
        {
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void run()
    {
        String fromServer = "";
        try
        {
            while(active)
            {
                fromServer = reader.readLine();
                playersPositions = fromServer.split("@");

                for (int i = 0; i < playersPositions.length; i++)
                {
                    String[] position = playersPositions[i].split(",");

                    if (Client.readyToUpdate && i == id)
                    {
                        Client.x = Integer.parseInt(position[1]);
                        Client.y = Integer.parseInt(position[2]);
                        Client.readyToUpdate = false;
                    }
                }

                fromServer = reader.readLine();
                String[] targetPositions = fromServer.split("#");
                int targetX =  Integer.parseInt(targetPositions[0]);
                int targetY =  Integer.parseInt(targetPositions[1]);

                if (targetX > 0 && targetY > 0)
                {
                    Client.targetExists = true;
                    Client.targetX = targetX;
                    Client.targetY = targetY;
                }

                Thread.sleep(30);
                lastPositions = playersPositions;

                String createTarget = Client.targetExists + "\n";
                writer.write(createTarget);
                writer.flush();

                String payload = Client.x + "," + Client.y + "\n";
                writer.write(payload);
                writer.flush();
            }
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            System.out.println("Closed socket!");
        }
    }
}
