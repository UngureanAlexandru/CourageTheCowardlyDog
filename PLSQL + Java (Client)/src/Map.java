import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;


public class Map {

    private int width;
    private int height;

    private int id;
    private GridPane root;

    private int playerX;
    private int playerY;

    Map(GridPane root, int width, int height)
    {
        this.width = width;
        this.height = height;
        this.id = id;
        this.root = root;

        for (int i = 0; i < width; i++)
        {
            for (int j = 1; j < height + 1; j++)
            {
                ImageView space = new ImageView(new Image("Space.png", 50, 50, false, true));
                root.add(space, i, j);
            }
        }
    }

    public void drawClient(int x, int y)
    {
        ImageView character = new ImageView(new Image("Courage.png", 50, 50, false, true));
        root.add(character, x , y);
    }

    public void drawOtherPLayer(int x, int y)
    {
        ImageView character = new ImageView(new Image("Eustache.jpg", 50, 50, false, true));
        root.add(character, x , y);
    }

    public void drawTarget(int x, int y)
    {
        ImageView character = new ImageView(new Image("Muriel.jpg", 50, 50, false, true));
        root.add(character, x , y);
    }

    public void erase(int x, int y)
    {
        ImageView space = new ImageView(new Image("Space.png", 50, 50, false, true));
        root.add(space, x , y);
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public void move(GridPane root, int x, int y, String key)
    {
        int lastX = x;
        int lastY = y;

        switch (key)
        {
            case "Up":
                lastY++;
                break;

            case "Left":
                lastX++;
                break;

            case "Down":
                lastY--;
                break;

            case "Right":
                lastX--;
                break;
        }

        if (isInside(lastX, lastY))
        {
            ImageView leftBehind;

            boolean found = false;

            if (x == lastX && y == lastY)
            {
                found = true;
            }

            if (found)
            {
                leftBehind = new ImageView(new Image("Eustache.jpg", 50, 50, false, true));
            }
            else
            {
                leftBehind = new ImageView(new Image("Space.png", 50, 50, false, true));
            }
            root.add(leftBehind, lastX , lastY);
        }

        if (isInside(x, y))
        {
            ImageView character = new ImageView(new Image("Courage.png", 50, 50, false, true));
            root.add(character, x , y);
            playerX = x;
            playerY = y;
        }
    }

    private boolean isInside(int x, int y)
    {
        if (x >= 0 && x < width && y > 0 && y < height)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
