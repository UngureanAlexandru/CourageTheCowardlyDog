import jdk.nashorn.internal.ir.CatchNode;

import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

public class Server{

    private Connection connection;
    private ServerSocket serverSocket;
    private PreparedStatement preparedStatement;
    private int PORT = 3000;

    public static ArrayList<Player> players;
    public static int targetX = -1;
    public static int targetY = -1;
    public static boolean targetExists = false;
    private int playerCounter = 0;

    private Random random;
    private final int mapWidth = 25;
    private final int mapHeight = 10;

    public static void main(String[] args)
    {
        Server server = new Server();
        server.init();
    }

    private void init()
    {
        players = new ArrayList<Player>();
        random = new Random();

        String url = "jdbc:oracle:thin:@localhost:1521:xe";
        String user = "STUDENT";
        String password = "STUDENT";

        try
        {
            serverSocket = new ServerSocket(PORT);

            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(url, user, password);
            preparedStatement = null;

            CreateTargetThread createTargetThread = new CreateTargetThread(this);
            new Thread(createTargetThread).start();

            while(true)
            {
                Socket client = serverSocket.accept();
                System.out.println("New connection!");

                Player player = new Player(playerCounter, 10, 5);
                playerCounter++;
                PlayerThread thread = new PlayerThread(player, client, this);
                new Thread(thread).start();

                players.add(player);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void createTarget()
    {
        targetX = random.nextInt(mapWidth);
        targetY = random.nextInt(mapHeight);
        System.out.println("Target created at: " + targetX + " " + targetY);
    }

    public void disconnectedUser(int id)
    {
        for (int i = 0; i < players.size(); i++)
        {
            if (players.get(i).getID() == id)
            {
                for (int j = i + 1; j < players.size(); j++)
                {
                    players.get(j - 1).copy(players.get(j));
                }
                break;
            }
        }
        playerCounter--;
        players.remove(playerCounter);
    }

    public String executeQuery(int id, String username, String password)
    {
        String result = "Error!";
        try
        {
            preparedStatement = connection.prepareStatement("insert into CourageGame (id, username, password) values (?, ?, ?)");
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, username);
            preparedStatement.setString(3, password);
            preparedStatement.execute();
            preparedStatement.close();

            preparedStatement = connection.prepareStatement("insert into CourageGameData (id, score, gamesplayed) values (?, 0, 0)");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            preparedStatement.close();
            result = "Success!";
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public String executeQuery(String username, String password)
    {
        String fromDatabase = "noData";

        try
        {
            String query = "select CourageGame.id, CourageGame.username, CourageGame.password, CourageGameData.score, CourageGameData.gamesplayed from CourageGame join CourageGameData on CourageGame.id = CourageGameData.id  where CourageGame.username = ? and CourageGame.password = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet result = preparedStatement.executeQuery();

            while(result.next())
            {
                fromDatabase = result.getInt("id") + ",";
                fromDatabase += result.getString("username") + ",";
                fromDatabase += result.getString("password") + ",";
                fromDatabase += result.getString("score") + ",";
                fromDatabase += result.getString("gamesplayed");
            }
                preparedStatement.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return fromDatabase;
    }

    public int getBiggestID()
    {
        int id = 0;

        try
        {
            String query = "select max(id) from CourageGame";
            preparedStatement = connection.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next())
            {
                id = result.getInt("max(id)");
            }
            preparedStatement.close();

        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

        return id;
    }

    public void save(int id, int score, int gamesPlayed)
    {
        try
        {
            String query = "update CourageGameData set score = ?, gamesPlayed = ? where id = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, score);
            preparedStatement.setInt(2, gamesPlayed);
            preparedStatement.setInt(3, id);
            int result = preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.commit();

            System.out.println("Saved: " + result);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

}
