
public class CreateTargetThread implements Runnable {

    private int number = 50;
    private Server server;

    CreateTargetThread(Server server)
    {
        this.server = server;
    }

    public void run()
    {
        while(true)
        {
            try
            {
                Thread.sleep(150);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            //System.out.println(Server.targetExists);

            if (!Server.targetExists)
            {
                Server.targetExists = true;
                server.createTarget();
            }
        }
    }
}
