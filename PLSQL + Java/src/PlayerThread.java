import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class PlayerThread implements Runnable {

    private Player player;
    Socket playerSocket;
    private boolean active = true;
    private BufferedWriter writer;
    private BufferedReader reader;
    private Server server;

    private int score;
    private int gamesPlayed;

    PlayerThread(Player player, Socket socket, Server server)
    {
        this.player = player;
        playerSocket = socket;
        this.server = server;
    }

    public void run()
    {
        boolean beforeGame = true;

        try
        {
            writer = new BufferedWriter(new OutputStreamWriter(playerSocket.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(playerSocket.getInputStream()));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        while(beforeGame)
        {
            try
            {
                String loginOrRegister = reader.readLine();
                String fromServer = reader.readLine();
                String[] data = fromServer.split(",");

                if (loginOrRegister.equals("login"))
                {
                    String result = server.executeQuery(data[0], data[1]);
                    writer.write(result + "\n");
                    writer.flush();

                    if (!result.equals("noData"))
                    {
                        data = result.split(",");
                        player.setId(Integer.parseInt(data[0]));
                        score = Integer.parseInt(data[3]);
                        gamesPlayed = Integer.parseInt(data[4]);
                        System.out.println("games played " + gamesPlayed);
                        beforeGame = false;
                    }
                }
                else if (loginOrRegister.equals("register"))
                {
                    int id = server.getBiggestID() + 1;
                    System.out.println("ID: " + id);
                    String result = server.executeQuery(id, data[0], data[1]);

                    writer.write(result + "\n");
                    writer.flush();
                    beforeGame = false;
                }
                loginOrRegister = "";
            }
            catch (Exception e)
            {
                //e.printStackTrace();
                System.out.println("Disconnected!");
                return;
            }
        }

        active = true;

        while(active)
        {
            try
            {
                Thread.sleep(50);

                String payload = "";

                for (int i = 0; i < Server.players.size(); i++)
                {
                    payload += Server.players.get(i).getID() + "," + Server.players.get(i).getX() + "," + Server.players.get(i).getY();

                    if (i != Server.players.size() - 1)
                    {
                        payload += "@";
                    }
                }
                System.out.println(payload);
                writer.write(payload + "\n");
                writer.flush();

                String targedPosition = "";

                targedPosition = Server.targetX + "#" + Server.targetY;

                writer.write(targedPosition + "\n");
                writer.flush();

                String fromServer = reader.readLine();

                if (!fromServer.contains(","))
                {
                    if (Server.targetExists)
                    {
                        Server.targetExists = Boolean.parseBoolean(fromServer);
                    }

                    String playerPos = reader.readLine();
                    String[] pos =  playerPos.split(",");
                    player.setPosition(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]));
                }
                else
                {
                    String[] data = fromServer.split(",");
                    score = Integer.parseInt(data[0]);
                    gamesPlayed = Integer.parseInt(data[1]);
                    server.save(player.getID(), score, gamesPlayed);
                }
            }
            catch (Exception e)
            {
                //e.printStackTrace();
                System.out.println("Disconnected");
                server.disconnectedUser(player.getID());
                active = false;
            }
        }
    }
}