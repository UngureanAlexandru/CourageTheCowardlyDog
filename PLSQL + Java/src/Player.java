public class Player {

    private int id;
    private int x;
    private int y;
    private int score = 0;

    Player(int id, int x, int y)
    {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void copy(Player player)
    {
        this.id = player.getID();
        this.x = player.getX();
        this.y = player.getY();
        this.score = player.getScore();
    }

    public void setPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getScore()
    {
        return score;
    }

    public int getID()
    {
        return id;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }
}
